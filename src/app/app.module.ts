import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgApexchartsModule } from '../../node_modules/ng-apexcharts';
import { AgmCoreModule } from '../../node_modules/@agm/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HamburgerMenuComponent } from './components/hamburger-menu/hamburger-menu.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { FilterByComponent } from './components/filter-by/filter-by.component';
import { BlocksDetailsComponent } from './components/blocks-details/blocks-details.component';
import { ChartComponent } from './components/chart/chart.component';
import { MapsComponent } from './components/maps/maps.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HamburgerMenuComponent,
    SearchbarComponent,
    TabsComponent,
    AboutPageComponent,
    FilterByComponent,
    BlocksDetailsComponent,
    HomeComponent,
    ChartComponent,
    MapsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgApexchartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAnXeFQLYjC5GU5ovFa4SBAxfbYu16zc2s&fbclid=IwAR3q0May41e97l_dZF0VPOgLIVxcETN3GCDwV-hx7hPLl5cBNPKbbZ2WPM8'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
