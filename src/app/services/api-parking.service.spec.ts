import { TestBed } from '@angular/core/testing';

import { ApiParkingService } from './api-parking.service';

describe('ApiParkingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiParkingService = TestBed.get(ApiParkingService);
    expect(service).toBeTruthy();
  });
});