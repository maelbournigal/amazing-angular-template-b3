import { City } from './../components/tabs/tabs.component';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TabService {
  tabObserver: any;
  tabChoose: any;

  constructor() {
    this.tabObserver = null;
    this.tabChoose = Observable.create(observer => {
      this.tabObserver = observer;
    });
  }

  public changeTabs(city: City) {
    if(this.tabObserver) {
      this.tabObserver.next(city);
    }
  }
}
