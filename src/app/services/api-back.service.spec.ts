import { TestBed } from '@angular/core/testing';

import { ApiBackService } from './api-back.service';

describe('ApiBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiBackService = TestBed.get(ApiBackService);
    expect(service).toBeTruthy();
  });
});
