import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiParkingService {

  constructor(private http: HttpClient) { }


  getDataFromNantes(): Observable<any>{
    return this.http.get(
      'https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes-disponibilites&rows=27&facet=grp_nom&facet=grp_statut'
      );
  }

  getParkingFromNantes(): Observable<any>{
    return this.http.get(
        'https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes&rows=27&facet=libcategorie&facet=libtype&facet=acces_pmr&facet=service_velo&facet=stationnement_velo&facet=stationnement_velo_securise&facet=moyen_paiement'
      )
  }

  getDataFromRennes(): Observable<any>{
    return this.http.get(
      'https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=export-api-parking-citedia'
    )
  }

  getDataFromAngers(): Observable<any> {
    return this.http.get(
      'https://data.angers.fr/api/records/1.0/search/?dataset=parking-angers&rows=18&facet=nom'
    );
  }

  getParkingFromAngers(): Observable<any> {
    return this.http.get(
      'https://data.angers.fr/api/records/1.0/search/?dataset=pv_equip_parking&facet=exploitant&facet=id_parking&facet=nb_places&rows=18&facet=nb_velo_secur&facet=nb_places_pmr'
    );
  }
}
