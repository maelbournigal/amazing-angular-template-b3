import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiBackService {

  constructor(private http: HttpClient) { }

  postParking(data): Observable<any>{
    return this.http.post('/parking', data);
  }

  getCity(): Observable<any>{
    return this.http.get('/city');
  }
}
