import { TabService } from './../../services/tab-service.service';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import ApexCharts from 'apexcharts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  chart: ApexCharts;
  @Input() type: string;
  @Input() parking: any = [];
  @Input() labelsY: any = [];
  @Input() labelsX: any = [];
  @Input() data: any = [];
  @Input() horizontal = false;
  constructor() {}

  ngOnInit(): void {
    let height = 1;
    if (this.parking.length > 0 ){
      height = this.parking.length;
    }
    // Gestion du graphique
    let options = {
      chart: {
        height: 30 * height,
        type: this.type,
        toolbar: {
          show: false
        }
      },
      // Nombre de places de parkings
      dataLabels: {
        enabled: true,
        textAnchor: 'start',
        offsetX: 15,
        style: {
          colors: ['#1a1a1a', '#1a1a1a'],
          fontSize: '10px'
        }
      },
      // Espacement entre les places de parkings
      stroke: {
        show: true,
        width: 1,
        colors: ['#fff']
      },
      plotOptions: {
        bar: {
          barHeight: '50%',
          horizontal: this.horizontal,
          dataLabels: {
            position: 'top',
            maxItems: 100,
            hideOverflowingLabels: true
          }
        },

      },
      series: this.makeSeries(),
      xaxis: this.makeXaxis(),
      tooltip: {
        y: {
          formatter(val) {
            return val;
          }
        }
      },
      fill: {
        opacity: 1
      },
      colors: ['#A3ACFF', 'rgba(0, 0, 0, 0.35)'],
      legend: {
        position: 'bottom',
        horizontalAlign: 'center',
        offsetX: 0,
        style: {
          fontSize: 16
        }
      }
    };
    this.chart = new ApexCharts(
      document.querySelector('#chart'),
      options
    );
    this.chart.render();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.chart) {
      this.chart.updateOptions({
        xaxis: this.makeXaxis(),
        series: this.makeSeries(),
        chart: this.makeChartOptions()
      })
    }
  }

  makeChartOptions() {
    let height = 1;
    if (this.parking.length > 0 ) {
      height = this.parking.length;
    }
    const chart = {
      height: 30 * height,
      type: this.type,
      toolbar: {
        show: false
      }
    }
    return chart;
  }

  makeSeries() {
    let places_occupees = this.data[0];
    var places_max = this.data[1];
    var series = [{
      name: 'Places occupées',
      data: places_occupees,
    },
    {
      name: 'Places maximum',
      data: places_max,
    }
    ];
    return series;
  }

  makeXaxis() {
    var xaxis = { 
      categories: this.labelsX,
      labels: {
        formatter(val) {
          return val;
        }
    }};
    return xaxis;
  }
}



