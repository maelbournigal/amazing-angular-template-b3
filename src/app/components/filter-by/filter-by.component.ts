import { Component, OnInit } from '@angular/core';

export interface Filter {
  id: string;
  option: string;
}

@Component({
  selector: 'app-filter-by',
  templateUrl: './filter-by.component.html',
  styleUrls: ['./filter-by.component.scss']
})
export class FilterByComponent implements OnInit {
	
  filters: Filter[] = [
    { id: 'option-1', option: 'Option non disponible' },
    { id: 'option-2', option: 'Les filtres ne sont pas disponibles' },
    { id: 'option-3', option: 'Option indisponible' },
    { id: 'option-4', option: 'Filtre manquant' }
  ];


  constructor() { }

  ngOnInit() {
  }

  toggleDropdown() {
    // Show dropdown
    document.getElementById('dropdown-menu').classList.toggle('active');

    // Display icons => arrow / cross / filter
    document.getElementsByClassName('arrow-filter')[0].classList.toggle('rotateArrow');
    document.getElementsByClassName('filter')[0].classList.toggle('invisible');
    document.getElementsByClassName('cross')[0].classList.toggle('visible');
  }

  selectedOption(id) {
  }

}
