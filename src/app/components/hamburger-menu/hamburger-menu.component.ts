import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hamburger-menu',
  templateUrl: './hamburger-menu.component.html',
  styleUrls: ['./hamburger-menu.component.scss']
})
export class HamburgerMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  toggleSidebar() {
    // Sidebar
    document.getElementById('sidebar').classList.toggle('active');

    // Hamburger menu
    document.getElementById('hamburger-menu').classList.toggle('active');
  }

}
