import { TabService } from './../../services/tab-service.service';
import { ApiBackService } from './../../services/api-back.service';
import { ApiParkingService } from './../../services/api-parking.service';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  subscription: Subscription;
  listParkingNantes: any[] = [];
  listParkingRennes: any[] = [];
  listParkingAngers: any[] = [];
  listAllParking: any[] = [];
  listParking: any[] = [];
  arrayObservable: any;
  arrayObserver: any;

  constructor(private apiServices: ApiParkingService, private apiBackService: ApiBackService, private tabService: TabService) {
    this.tabService.tabChoose.subscribe(city => {
      if (city) {
        switch (city.name) {
          case 'Nantes':
            this.listParking = this.listParkingNantes
            break;
          case 'Angers':
            this.listParking = this.listParkingAngers
            break;
          case 'Rennes':
            this.listParking = this.listParkingRennes;
            break;
          default:
            this.listParking = this.listAllParking;
            break;
        }
      } else {
        this.listParking = this.listAllParking;
      }
    });
  }

  ngOnInit() {
    this.nantes();
    this.rennes();
    this.angers();
  }

  nantes() {
    let arrayNantesName = [];
    let arrayNantesIdObjects = [];
    let arrayNantesId = [];
    let arrayDisponibleNantes = [];
    let arrayDateNantes = [];
    this.getDataFromNantes()
      .then(res => {
        arrayDateNantes = res.records.map(records => records.fields.grp_horodatage)
        arrayNantesIdObjects = res.records.map(records => records.fields.idobj);
        arrayNantesId = res.records.map(records => records.recordid);
        arrayNantesName = res.records.map(records => records.fields.grp_nom);
        arrayDisponibleNantes = res.records.map(records => records.fields.grp_disponible);
      }).catch(err => console.log(err));

    let arrayParkingNantesIdObjects = [];
    let arrayMaxPlaceNantes = [];
    let arrayGeoNantes = [];
    let arrayPmrNantes = [];
    this.getParkingFromNantes().then(res => {
      arrayPmrNantes = res.records.map(records => records.fields.capacite_pmr);
      arrayParkingNantesIdObjects = res.records.map(records => records.fields.idobj);
      arrayMaxPlaceNantes = res.records.map(records => records.fields.capacite_voiture);
      arrayGeoNantes = res.records.map(records => records.geometry.coordinates);
      this.generateListParkingNantes(arrayNantesIdObjects, arrayNantesName, arrayNantesId, arrayDisponibleNantes, arrayMaxPlaceNantes,
        arrayGeoNantes, arrayParkingNantesIdObjects, arrayDateNantes, arrayPmrNantes);
    }).catch(err => console.error(err))
  }

  rennes() {
    this.getDataFromRennes()
      .then(res => {
        this.listParkingRennes = [];
        res.records.map(records => {
          this.listParkingRennes.push(
            {
              recordid: records.recordid,
              nom: records.fields.key,
              place_max: records.fields.max,
              place_occupe: records.fields.max - records.fields.free,
              pourcentage_occupe: (records.fields.free / records.fields.max) * 100,
              ville: 'Rennes',
              date: records.record_timestamp,
              coordonnees: records.geometry.coordinates
            }
          )
        })
      }).catch(err => console.log(err));
  }

  angers() {
    let arrayId = [];
    let arrayName = [];
    let arrayDispo = [];
    let arrayDate = [];
    this.getDataFromAngers()
      .then(res => {
        arrayId = res.records.map(record => record.recordid);
        arrayName = res.records.map(record => record.fields.nom);
        arrayDispo = res.records.map(record => record.fields.disponible);
        arrayDate = res.records.map(record => record.record_timestamp);
      }).catch(err => console.log(err));

    let arrayNameParking = [];
    let arrayNbPlace = [];
    let arrayCoordinates = [];
    let arrayPlacesPmrAngers = [];
    this.getParkingFromAngers()
      .then(res => {
        arrayNameParking = res.records.map(record => record.fields.id_parking);
        arrayNbPlace = res.records.map(record => record.fields.nb_places);
        arrayCoordinates = res.records.map(record => record.geometry.coordinates);
        arrayPlacesPmrAngers = res.records.map(record => record.fields.nb_places_pmr);
        this.generateListParkingAnger(arrayName, arrayNameParking, arrayNbPlace, arrayCoordinates,
          arrayDate, arrayDispo, arrayId, arrayPlacesPmrAngers);
      }).catch(err => console.log(err));
  }

  generateListParkingAnger(arrayName, arrayNameParking, arrayNbPlace, arrayCoordinates, arrayDate, arrayDispo, arrayId, arrayPmr) {
    arrayName.forEach((name, index) => {
      const indexArrayParking = arrayNameParking.indexOf(name);
      const parking = {
        recordid: arrayId[index],
        nom: name,
        place_max: arrayNbPlace[indexArrayParking],
        place_occupe: arrayNbPlace[indexArrayParking] - arrayDispo[index],
        pourcentage_occupe: 100 - (arrayDispo[index] / arrayNbPlace[indexArrayParking]) * 100,
        ville: 'Angers',
        date: arrayDate[index],
        coordonnees: arrayCoordinates[indexArrayParking],
        pmr: arrayPmr[indexArrayParking]
      };
      this.listParkingAngers.push(parking);
      this.listParking.push(parking);
      this.listAllParking.push(parking);
    });
    setTimeout(() => {
      this.postAllParking();
    }, 1000);
  }

  generateListParkingNantes(arrayObjId, arrayName, arrayNantesId, arrayDispo, arrayMax, arrayGeo, arrayObjIdParking, arrayDate, arrayPmr) {
    arrayObjId.forEach((objId, index) => {
      const indexArrayParking = arrayObjIdParking.indexOf(objId);
      const parking = {
        recordid: arrayNantesId[index],
        nom: arrayName[index],
        place_max: arrayMax[indexArrayParking],
        place_occupe: arrayMax[indexArrayParking] - arrayDispo[index],
        pourcentage_occupe: 100 - ((arrayMax[indexArrayParking] - arrayDispo[index]) / arrayMax[indexArrayParking]) * 100,
        ville: 'Nantes',
        date: arrayDate[index],
        coordonnees: arrayGeo[indexArrayParking],
        pmr: arrayPmr[indexArrayParking]
      }
      this.listParkingNantes.push(parking);
    });
  }

  postAllParking() {
    let data = [];
    this.listParkingAngers.forEach(parking => {
      if (!parking.place_occupe) {
        parking.place_occupe = 0;
      }
      if (!parking.place_max) {
        parking.place_max = 0;
        parking.pourcentage_occupe = 0;
      }
      if (!parking.pourcentage_occupe) {
        parking.pourcentage_occupe = 0;
      }
      if (!parking.coordonnees) {
        parking.coordonnees = '[0,0]';
      }
      data.push(parking)
    });

    this.listParkingNantes.forEach(parking => {
      if (!parking.place_occupe) {
        parking.place_occupe = 0;
      }
      if (!parking.place_max) {
        parking.place_max = 0;
        parking.pourcentage_occupe = 0;
      }
      if (!parking.pourcentage_occupe) {
        parking.pourcentage_occupe = 0;
      }
      if (!parking.coordonnees) {
        parking.coordonnees = '[0,0]';
      }
      data.push(parking);
      this.listParking.push(parking);
      this.listAllParking.push(parking);
    });

    this.listParkingRennes.forEach((parking) => {

      if (!parking.place_occupe) {
        parking.place_occupe = 0;
      }
      if (!parking.place_max) {
        parking.place_max = 0;
        parking.pourcentage_occupe = 0;
      }
      if (!parking.pourcentage_occupe) {
        parking.pourcentage_occupe = 0;
      }
      if (!parking.coordonnees) {
        parking.coordonnees = '[0,0]';
      }
      data.push(parking);
      this.listParking.push(parking);
      this.listAllParking.push(parking);
    })

    setTimeout(() => {
    this.postParking(data).then((res) => {})
      .catch(err => console.log(err));
    }, 1000);
  }

  getDataFromNantes(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiServices.getDataFromNantes()
        .subscribe(res => resolve(res), err => reject(err));
    });
  }

  getParkingFromNantes(): Promise<any>{
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiServices.getParkingFromNantes()
        .subscribe(res => resolve(res), err => reject(err));
    })
  }

  getDataFromRennes(): Promise<any>{
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiServices.getDataFromRennes()
        .subscribe(res => resolve(res), err => reject(err));
    });
  }

  getDataFromAngers(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiServices.getDataFromAngers()
        .subscribe(res => resolve(res), err => reject(err));
    });
  }

  getParkingFromAngers(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiServices.getParkingFromAngers()
        .subscribe(res => resolve(res), err => reject(err));
    });
  }

  postParking(data): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiBackService.postParking(data)
        .subscribe(res => resolve(res), err => reject(err));
    });
  }
}
