import { TabService } from './../../services/tab-service.service';
import { Subscription } from 'rxjs';
import { ApiBackService } from './../../services/api-back.service';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

export interface City {
  id: string;
  name: string;
  coordonnates: any;
}

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  cities: City[] = [];
  subscription: Subscription;
  citySelected: City;
  listParkingName;
  listData = [];
  home = 1;
  @Input() listParking;

  constructor(private apiBackService: ApiBackService, private tabService: TabService) {
  }

  ngOnInit() {
    this.getCity();
  }

  ngOnChanges(changes: SimpleChanges): void {
   this.listParkingName = this.listParking.map(parking => parking.nom.toLowerCase().charAt(0).toUpperCase() + parking.nom.toLowerCase().slice(1));
   this.listData = [];
   this.listData.push(this.listParking.map(parking => parking.place_occupe));
   this.listData.push(this.listParking.map(parking => parking.place_max));
  }

  getCity() {
    this.getCityObserver().then(res => {
      this.cities = res;
      this.cities.forEach(city => {
        if (city.coordonnates) {
          city.coordonnates = JSON.parse(city.coordonnates).replace('[', '').replace(']', '').split(',');
        }
      });
      const cityNameSort = this.cities.map(city => city.name).sort();
      const cityName = this.cities.map(city => city.name);

      let array = [];
      cityNameSort.forEach(name => {
        const index = cityName.indexOf(name);
        array.push(this.cities[index]);
      })
      this.cities = array;
    }).catch(err => console.log(err))
  }

  getCityObserver(): Promise<any>{
    return new Promise<any>((resolve, reject) => {
      this.subscription = this.apiBackService.getCity()
      .subscribe(res => resolve(res), err => reject(err))
    });
  }

  selectTab(city: City) {
    this.home = 0;
    this.tabService.changeTabs(city);
    this.citySelected = city;
  }

  goHome() {
    this.home = 1;
    this.citySelected =  null;
    this.tabService.changeTabs(null);
  }
}
