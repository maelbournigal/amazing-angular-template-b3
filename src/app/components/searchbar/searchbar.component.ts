import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  filterParking() {

    const input = document.getElementById('input-search');
    const filter = (<HTMLInputElement>input).value.toUpperCase();
    const item = document.getElementsByClassName('nav-item') as HTMLCollectionOf<HTMLElement>;
    
    for (var i = 0; i < item.length; i++) {
      let txtValue = item[i].textContent || item[i].innerText;

      if (txtValue.toUpperCase().indexOf(filter) > -1)
        item[i].style.display = '';
      else
        item[i].style.display = 'none';
    }
    
  }

}
