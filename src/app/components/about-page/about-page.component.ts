import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { City } from '../tabs/tabs.component';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.scss']
})
export class AboutPageComponent {

  arrayVoyelle = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y'];
  @Input() city: City;
  @Input() listParking;
  listData;
  listParkingName;
  listCoordonates;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.listParkingName = this.listParking.map(parking => parking.nom.toLowerCase().charAt(0).toUpperCase() + parking.nom.toLowerCase().slice(1));
    this.listData = [];
    this.listData.push(this.listParking.map(parking => parking.place_occupe));
    this.listData.push(this.listParking.map(parking => parking.place_max));
    this.listCoordonates = this.listParking.map(parking => parking.coordonnees);
  }
}
