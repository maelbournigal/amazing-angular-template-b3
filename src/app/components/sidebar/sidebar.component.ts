import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() listParking: any[] = [];
  constructor() {
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    const parkingNameSort = this.listParking.map(parking => parking.nom).sort();
    const parkingName = this.listParking.map(parking => parking.nom);
    let list = [];
    if (parkingNameSort.length > 0) {
      parkingNameSort.forEach(name => {
        const index = parkingName.indexOf(name);
        list.push(this.listParking[index]);
      })
      this.listParking = list;
    }
  }
}
