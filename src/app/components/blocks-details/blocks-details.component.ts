import { Component, OnInit, Input } from '@angular/core';
import { isNumber } from 'util';

@Component({
  selector: 'app-blocks-details',
  templateUrl: './blocks-details.component.html',
  styleUrls: ['./blocks-details.component.scss']
})
export class BlocksDetailsComponent implements OnInit {

  @Input() listParking;
  placeMax: any = '?';
  placeOccupe: any = '?';
  pourcent: any = '?';
  placePmr: any = '?';
  constructor() {

  }

  ngOnInit() {

  }

  ngOnChanges(): void {
    this.placeMax = '?';
    let placeMax = this.listParking.map(parking => parking.place_max);
    if (placeMax.length > 0){
      this.placeMax = 0;
      placeMax.forEach((max: number) => {
        if (max) {
          this.placeMax += max;
        }
    });
    }

    this.placeOccupe = '?';
    let placeOccupe = this.listParking.map(parking => parking.place_occupe);
    if (placeOccupe.length > 0){
      this.placeOccupe = 0;
      placeOccupe.forEach((occupe: number) => {
        if (occupe) {
          this.placeOccupe += occupe;
        }
      });
    }

    this.placePmr = '?';
    let listPmr = this.listParking.map(parking => parking.pmr)
    if (listPmr[0] || listPmr[1] || listPmr[2]) {
      this.placePmr = 0;
      listPmr.forEach(pmr => {
        if (pmr) {
          this.placePmr += pmr;
        }
      });
    }

    this.pourcent = '?';
    if (isNumber(this.placeOccupe) && isNumber(this.placeMax)) {
      this.pourcent = (this.placeOccupe / this.placeMax) * 100;
    }
  }

}
