import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocksDetailsComponent } from './blocks-details.component';

describe('BlocksDetailsComponent', () => {
  let component: BlocksDetailsComponent;
  let fixture: ComponentFixture<BlocksDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlocksDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocksDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
