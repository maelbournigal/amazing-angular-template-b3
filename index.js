const express = require('express');
const app = express();
const mysql = require('mysql');
const cors = require('cors');
const path = require('path');

let mysqlClient;
function getConnection() {
    mysqlClient = mysql.createConnection({
        host: "us-cdbr-iron-east-02.cleardb.net",
        user: "b2f12faf3c4de3",
        password: "7b6e7918",
        database: "heroku_c34109f2f1dc461"
    });

    mysqlClient.connect(function(err) {
        if (err) {
            console.log('error when connecting to db:', err);
            setTimeout(getConnection, 2000);
        }
    })

    mysqlClient.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
          getConnection();
        } else {
          throw err;
        }
      });
}

let listRecordsId = [];
function getRecords() {
    let selectQuery = "SELECT recordid FROM record";
    return new Promise((resolve, reject) => {
        mysqlClient.query(
            selectQuery,
            function (error, results, fields) {
                if(error) {
                    console.log(error);
                    reject(error);
                }
                results = JSON.stringify(results);
                listRecordsId = [];
                JSON.parse(results).forEach(record => {
                    listRecordsId.push(record.recordid);
                });
                resolve(listRecordsId);
            }
        );
    });
}

let listCity = [];
function getCity() {
    let selectQuery = "SELECT * FROM ville";
    return new Promise((resolve,reject) => {
        mysqlClient.query(
            selectQuery,
            function (error, results, fields) {
                if(error) {
                    reject(error);
                }else {
                    results = JSON.stringify(results);
                    listCity = JSON.parse(results);
                    resolve(listCity)
                }
            }
        );
    });
}

app.use(cors());
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/dist/amazing-angular-project'))

app.get('/parking/:date', function (req, res) {
    const date = req.params.date;
    res.send(date);
});

app.get('/parking/:datefrom/:dateto', function(req, res) {
    const datefrom = req.params.datefrom;
    const dateto = req.params.dateto;
    res.send(datefrom + '/' + dateto);
});

app.get('/city', function(req, res) {
    getConnection();
    getCity().then(results => {
        res.json(results)
    })
})

app.post('/parking', function(req, res, next) {
    let arrayInsert = [];
    let postRecordId;
    getConnection();
    getCity().then(resultsCity => {
        getRecords().then(results => {
            let requestPrepareRecords = "INSERT INTO record(recordid) VALUES (?)";
            req.body.forEach(record => {
                if(results.indexOf(record.recordid) === -1) {
                    const data = [record.recordid];
                    mysqlClient.query(
                        requestPrepareRecords,
                        data,
                        function post(error, results, fields) {
                            if(error) {
                                console.log(error);
                                return;
                            }
                            if(results) {
                                results = JSON.stringify(results);
                            }
                        }
                    );
                    arrayInsert.push(record);
                    const requestPreparePost = 'INSERT INTO parking(nom, place_max, place_occupe, pourcentage_occupe, id_ville, date, coordonnees) VALUES (?, ?, ?, ?, ?, ?, ?)';
                    arrayInsert.forEach((parking) => {
                        let id_ville;
                        listCity.forEach(city => {
                            if(parking.ville === city.name) {
                                id_ville = city.id;
                            }
                        });
                        let date = new Date(parking.date);
                        const coordonnees = JSON.stringify(parking.coordonnees);
                        let data = [parking.nom, parking.place_max, parking.place_occupe, parking.pourcentage_occupe, id_ville, date, coordonnees]
                        mysqlClient.query(
                            requestPreparePost, data, function post(error, results, fields) {
                                if(error) {
                                    console.log(data);
                                    console.error(error);
                                    return;
                                }
                                if(results) {
                                    results = JSON.stringify(results);
                                }
                            }
                        );
                    });
                }
            });
            mysqlClient.end();
            res.json(arrayInsert);
        });
    });
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname + '/dist/amazing-angular-project/index.html'));
});

const port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log(`Exemple app listening on port ${port} !`);
});